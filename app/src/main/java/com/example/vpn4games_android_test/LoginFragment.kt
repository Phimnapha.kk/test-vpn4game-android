package com.example.vpn4games_android_test

import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import com.example.vpn4games_android_test.api.APIService
import com.example.vpn4games_android_test.api.HashUtils
import com.example.vpn4games_android_test.models.UserResponse
import com.example.vpn4games_android_test.utils.SingleLiveData
import com.example.vpn4games_android_test.viewmodels.LoginViewModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import okhttp3.*
import retrofit2.Retrofit
import java.io.IOException
import java.lang.ref.WeakReference
import java.nio.channels.Selector


public val datauser = arrayListOf<User>()
public val KEY_PREFS= "key_prefs"

class LoginFragment : Fragment() {
    private val loginViewModel: LoginViewModel by activityViewModels()
    private lateinit var itemSelector: Selector

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        progressBar_login?.visibility = View.GONE

        //get username show
        val shared: SharedPreferences = requireActivity().getSharedPreferences(KEY_PREFS, Context.MODE_PRIVATE)
        val usernameValue: String = shared.getString("key_username", "").toString()
        txtUsername.setText(usernameValue)

        //Login
        btn_ClickLogin.setOnClickListener{
            hideKeyboard()
            val username = txtUsername.text.toString().trim()
            val password = txtPassword.text.toString().trim()
            if(username.isBlank())
            {
                Toast.makeText(context,R.string.error_username_required, Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if(password.isBlank())
            {
                Toast.makeText(context,R.string.error_password_required, Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            progressBar_login?.visibility = View.VISIBLE
            loginViewModel.login(username,password).observe(this , response)
        }

        //Singup
        btn_ClickSignup.setOnClickListener {
            val goSignup = Intent(activity, SignupActivity::class.java)
            startActivity(goSignup)
        }
        return view
    }

    private val response = Observer<String?> {response ->
        progressBar_login?.visibility = View.GONE
        if(response == null) return@Observer
        /*if(response != "200")
           Toast.makeText(context,"Something went wrong! Please try again", Toast.LENGTH_SHORT).show()
        else */

        //val gson = Gson()
        //val gson = GsonBuilder().create()
        //val userresponse = gson.fromJson(response, UserResponse::class.java )

        println("Work This")
        Toast.makeText(context, response, Toast.LENGTH_SHORT).show()
    }

    private fun onClickLogin() {

    }

    private fun toMain(){
        val intent = Intent(activity, MainActivity2::class.java)
       //intent.putExtra("username",username)
        println("Start Activity Dashboard")
        startActivity(intent)
    }

    private fun hideKeyboard() {
        activity?.let { activity ->
            activity.currentFocus?.let {
                val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(it.windowToken, 0)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun fetchJson() {
        val context = WeakReference(context).get() ?: throw Exception("context is null")
        val apiParams = HashMap<String, String>()

        val client = OkHttpClient()
        //val url = "https://api.ipify.org/?format=json"
        val url = "https://stg-api.alphagame.co/v2/services/remote-ip"
        val formbody = HashUtils().generateFormBody(apiParams, "", "")
        //val formBody = context?.let { HashUtils().generateFormBody(it, apiParams, "", "") }

        var usertoken = HashUtils().generateUserToken("berryP","123456")
        println("usertoken : $usertoken")

        var nonce = HashUtils().generateNonce()
        val request = Request.Builder()
                .url(url)
                .post(formbody)
                .build()

        //response1
        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response?.body()?.string()
                println("response : $body  nonce : $nonce  generate : $formbody  user token : $usertoken" )
                println("try and get json api working there was an error")
            }

            override fun onFailure(call: Call, e: IOException) {
                println("failed to execute request")
            }

        })

        //response2 must use doInbackground Asynctask
        /*try {
            var response: Response? = null
            response = client.newCall(request).execute()
            var body = response.body()
            println("response2 : $body")
        }catch (e:IOException)
        {
            println("Error : "+e.message)
        }*/
    }
}
