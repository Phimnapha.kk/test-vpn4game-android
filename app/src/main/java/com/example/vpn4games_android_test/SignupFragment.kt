package com.example.vpn4games_android_test

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_signup.*

class SignupFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.fragment_signup, container, false)
        btn_ClickSignup.setOnClickListener {
           Toast.makeText(context,"Please Signup",Toast.LENGTH_SHORT).show()
        }

        btn_ClickLogin.setOnClickListener {
            val goLogin = Intent(activity, Login::class.java)
            startActivity(goLogin)
        }
        return view
    }

}