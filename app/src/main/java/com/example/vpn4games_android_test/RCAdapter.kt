package com.example.vpn4games_android_test

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import org.w3c.dom.Text

//class RCAdapter(val arrayList: ArrayList<Model>, val context: Context) : RecyclerView.Adapter<RCAdapter.ViewHolder> {
class RCAdapter : RecyclerView.Adapter<RCAdapter.ViewHolder>() {

    private var server = arrayOf("Japan 1","Japan 2", "Japan 3")
    private val image_flag = arrayOf(R.drawable.flag_japan, R.drawable.flag_japan, R.drawable.flag_japan)
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        /*fun bindItems(model:Model){
            itemView.name_server.text
        }*/
        var itemImage: ImageView
        var itemServer: TextView

        init {
            itemImage = itemView.findViewById(R.id.name_server)
            itemServer = itemView.findViewById(R.id.image_flag)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemServer.text = server[position]
        holder.itemImage.setImageResource(image_flag[position])
    }

    override fun getItemCount(): Int {
        return server.size
    }
}