package com.example.vpn4games_android_test.viewmodels

import android.app.Application
import android.content.ClipData
import android.content.Intent
import android.util.Log
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.vpn4games_android_test.LoginFragment
import com.example.vpn4games_android_test.MainActivity2
import com.example.vpn4games_android_test.api.AuthenServiceResponse
import com.example.vpn4games_android_test.api.ErrorServiceResponse
import com.example.vpn4games_android_test.api.HashUtils
import com.example.vpn4games_android_test.api.ResponseAuthen
import com.example.vpn4games_android_test.models.UserResponse
import com.example.vpn4games_android_test.utils.SingleLiveData
import com.google.gson.GsonBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.*
import java.io.IOException
import java.util.HashMap


class LoginViewModel(application: Application): AndroidViewModel(application){

    fun login(username:String, password:String) : SingleLiveData<String> {
        val getResponse = SingleLiveData<String>()
        viewModelScope.launch {
            getResponse.value = requestAuthen(username,password)
            ResponseAuthen().cacheAuthen(getApplication(),username)
            Log.d("login","LoginViewModel access")
            println("response 2 : ${getResponse.value.toString()}")
        }
        return getResponse
    }

    private suspend fun requestAuthen(username:String, password:String) : String{
        return withContext(Dispatchers.IO) {
            var nonce = HashUtils().generateNonce()
            val apiParams = HashMap<String, String>()
            val client = OkHttpClient()
            val url = "https://stg-api.alphagame.co/v2/authen"
            val formbody = HashUtils().generateFormBody(apiParams, username, password)

            val request = Request.Builder()
                .url(url)
                .post(formbody)
                .build()

            try
            {
                var response: Response? = null
                response = client.newCall(request).execute()
                if(response.isSuccessful)
                {
                    val body = response.body()?.string().toString()
                    val gson = GsonBuilder().create()
                    val authenresponse = gson.fromJson(body, AuthenServiceResponse::class.java)
                    Log.d("authen username",authenresponse.username.toString())
                    body
                }
                else
                {
                    val body = response.body()?.string().toString()
                    val gson = GsonBuilder().create()
                    val errorponse = gson.fromJson(body, ErrorServiceResponse::class.java)
                    "Something went wrong. Please try again."
                }
            }catch (e:Exception){
                "Error Code ${e.message}"
            }
        }
    }

}