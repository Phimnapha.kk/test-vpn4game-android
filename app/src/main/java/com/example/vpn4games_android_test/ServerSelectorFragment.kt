package com.example.vpn4games_android_test

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class ServerSelectorFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_server_selector, container, false)
        var shared: SharedPreferences = activity!!.getSharedPreferences(KEY_PREFS,Context.MODE_PRIVATE)
        var server = view.findViewById<TextView>(R.id.txtServer)
        server.setText(shared.getString("key_lastserver","Server"))
        view.setOnClickListener{
            val intent = Intent(activity, ServerActivity::class.java)
            startActivityForResult(intent,1)
        }
        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == RESULT_OK){
            var txtserver:TextView = view!!.findViewById(R.id.txtServer)
            txtserver.setText(data?.getStringExtra("Server"))
        }
    }


}