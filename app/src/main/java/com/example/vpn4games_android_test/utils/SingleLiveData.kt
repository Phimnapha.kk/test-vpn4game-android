package com.example.vpn4games_android_test.utils

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

class SingleLiveData<T> : MutableLiveData<T?>() {
    override fun observe(owner: LifecycleOwner, observer: Observer<in T?>) {
        super.observe(owner, Observer {t ->
            t?.let {
                observer.onChanged(it)
                postValue(null)
            }
        })
    }
}