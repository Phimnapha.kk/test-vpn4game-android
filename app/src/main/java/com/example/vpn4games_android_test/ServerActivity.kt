package com.example.vpn4games_android_test

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ServerActivity : AppCompatActivity() {
    var dataArray = arrayListOf<ItemServer>()

    private var layoutmanager: RecyclerView.LayoutManager? = null
    private var adapter: RecyclerView.Adapter<RCAdapter.ViewHolder>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_server)
        //recyclerView
        /*val recyclerView_ = findViewById<RecyclerView.RecyclerListener
                >(R.id.recyclerView).apply {
            adapter = username_
        }*/

       // adapter = RCAdapter()
        /*layoutmanager = LinearLayoutManager(this)
        var recyclerView_ = findViewById<RecyclerView>(R.id.recyclerView).apply {
            layoutManager = layoutmanager
            adapter = RCAdapter()
        }*/

        layoutmanager = LinearLayoutManager(this)
        var recyclerView_ = findViewById<RecyclerView>(R.id.recyclerView).apply {
            layoutManager = layoutmanager
            adapter = CustomAdapter()
        }
        addData()
    }

    private fun addData(){
        for(i in 1..5){
            dataArray.add(ItemServer("Japan $i", R.drawable.flag_japan))
        }
        //refresh
       var recyclerView_ = findViewById<RecyclerView>(R.id.recyclerView).adapter!!.notifyDataSetChanged()
    }

    inner class CustomAdapter : RecyclerView.Adapter<CustomHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomHolder {
            val view = LayoutInflater.from(applicationContext).inflate(R.layout.list_item,parent,false)
            return CustomHolder(view)
        }

        override fun onBindViewHolder(holder: CustomHolder, position: Int) {
            holder.itemServer.text = dataArray[position].servername
            holder.itemImage.setImageResource(dataArray[position].image)

           /* val intent = Intent(context, MainActivity2::class.java)
            holder.itemView.setOnClickListener{
                val model = dataArray.get(position)
                var select_server :String = model.servername

            }*/

        }

        override fun getItemCount(): Int {
            return dataArray.size
        }

    }
    inner class CustomHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var itemImage: ImageView = itemView.findViewById(R.id.image_flag)
        var itemServer: TextView = itemView.findViewById(R.id.name_server)

        init{
            itemView.setOnClickListener{ v: View ->
                val position:Int = adapterPosition
                Toast.makeText(itemView.context,dataArray[position].servername,Toast.LENGTH_SHORT).show()

                //save lastserver to sharedpreference
                val shared: SharedPreferences = getSharedPreferences(KEY_PREFS, Context.MODE_PRIVATE)
                val editor = shared.edit()
                editor.putString("key_lastserver",dataArray[position].servername.toString())
                editor.commit()

                //send data sever select back to dashboard
                val intent = Intent()
                intent.putExtra("Server", dataArray[position].servername.toString())
                setResult(RESULT_OK, intent)
                finish()
            }
        }
    }
}

