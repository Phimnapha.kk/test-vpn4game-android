package com.example.vpn4games_android_test

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class AccountWidgetFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /*
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }*/
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_account_widget, container, false)
        var username_:String? ="";
       /* if(username_ == "")
            username_ = datauser[0].username.toString()*/

        val shared: SharedPreferences = activity!!.getSharedPreferences(KEY_PREFS, Context.MODE_PRIVATE)
        username_= shared.getString("key_username",null)
        val show_username = view.findViewById<TextView>(R.id.mUsername).apply {
            text = username_
        }
        return view
    }

}