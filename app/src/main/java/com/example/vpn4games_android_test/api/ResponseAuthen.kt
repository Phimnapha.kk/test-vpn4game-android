package com.example.vpn4games_android_test.api

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import androidx.annotation.RequiresApi
import com.example.vpn4games_android_test.KEY_PREFS
import okhttp3.*
import java.io.IOException
import java.lang.ref.WeakReference
import java.util.HashMap

class ResponseAuthen() {
    suspend fun requestAuthen(){

    }

    fun cacheAuthen(context: Context, cacheusername:String)
    {
        val shared: SharedPreferences = context.getSharedPreferences(KEY_PREFS,Context.MODE_PRIVATE)
        val editor = shared.edit()
        editor.putString("key_username", cacheusername)
        editor.commit()
    }

}