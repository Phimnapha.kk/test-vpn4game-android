package com.example.vpn4games_android_test.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ErrorServiceResponse {
    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = null
}