package com.example.vpn4games_android_test.api

import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface APIService {
    @Headers("Content-Type: application/json")
    @POST("/?format=json")
    suspend fun authen(@Body authendata: RequestBody): Call<ResponseBody>

}