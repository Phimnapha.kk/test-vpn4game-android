package com.example.vpn4games_android_test.api

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

class AuthenServiceResponse {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("username")
    @Expose
    var username: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("confirm")
    @Expose
    var confirm: Int? = null

    @SerializedName("active")
    @Expose
    var active: Int? = null

    @SerializedName("start")
    @Expose
    var start: String? = null

    @SerializedName("end")
    @Expose
    var end: String? = null

    @SerializedName("is_trial")
    @Expose
    var isTrial: Int? = null

    @SerializedName("is_pay")
    @Expose
    var isPay: Int? = null

    @SerializedName("is_connect")
    @Expose
    var isConnect: Int? = null

    @SerializedName("type")
    @Expose
    var type: Any? = null

    @SerializedName("connection")
    @Expose
    var connection: Int? = null

    @SerializedName("ref_id")
    @Expose
    var refId: Int? = null

    @SerializedName("datetime")
    @Expose
    var datetime: String? = null

    @SerializedName("phone")
    @Expose
    var phone: Any? = null
}