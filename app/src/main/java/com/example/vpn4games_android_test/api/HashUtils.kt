package com.example.vpn4games_android_test.api

import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.provider.Settings.Global.getString
import androidx.annotation.RequiresApi
import com.example.vpn4games_android_test.R
import com.google.common.hash.Hashing
import okhttp3.FormBody
import okio.HashingSink.hmacSha256
import java.lang.ref.WeakReference
import java.nio.charset.Charset
import java.util.*

class HashUtils {
    //private var secretKey: String = Resources.getSystem().getString(R.string.secret_key).toString()
    private val secretKey: String
    //var resource: Resources =
    init {
        this.secretKey = "d3Vn@/!D-U+yR[:*"
    }
   @RequiresApi(value = 21)
   fun generateUserToken(username:String, password:String): String{
       return generateSha1(generateSha1(password.toLowerCase()) + secretKey + username.toLowerCase())
   }

    fun generateNonce(): String {
        val date = Date()
        val time = date.time
        return time.toString()
    }

    @RequiresApi(value = 21)
    fun generateFormBody(data: HashMap<String, String>, username: String, password: String): FormBody {
       // val signatureUtils = HashUtils(context)
        //val userToken = signatureUtils.generateUserToken(username, password)
        var usertoken = generateUserToken(username,password)
        println("generate token : $username  $password  $usertoken")
        data["user_token"] = usertoken
        return doGenerateFormBody(data)
    }

    fun doGenerateFormBody(data: HashMap<String, String>): FormBody {
        data["nonce"] = generateNonce()
        data["client_os"] = "android"
        val signature = generateSignature(data)
        data["signature"] = signature
        return convertHashMap2FormBody(data)
    }

    fun generateSignature(map: HashMap<String, String>): String {
        var concat = StringBuilder()
        val sorted = TreeMap(map)
        for ((key, value) in sorted) {
            concat.append(key).append(value)
        }
        concat = StringBuilder(concat.toString().toLowerCase())
        return generateSha256(concat.toString())
    }

    private fun generateSha256(data: String): String {
        return Hashing.hmacSha256(secretKey.toByteArray())
            .hashString(data, Charset.forName("UTF-8"))
            .toString()
    }

    private fun generateSha1(data: String):String {
        return  Hashing.sha1()
                .hashString(data, Charset.forName("UTF-8"))
                .toString()
    }

    fun convertHashMap2FormBody(hashMap: HashMap<String, String>): FormBody {
        val builder = FormBody.Builder()
        for ((key, value) in hashMap) {
            builder.add(key, value)
        }
        return builder.build()
    }
}